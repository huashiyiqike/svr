for i=1:6
        tmp=sprintf('Angle Estimation\\fr00%d.bmp',i);
        img=imread(tmp);
     if i<5
        [y,x]=find(img~=0);
        imshow(img)
        hold on
        x=x';y=y';
        %scatter(y,x);
        %[a,astar,b]=mysvr(x,y,ex,c,tol,max_passes)
        [a,astar,b]=svr(x,y,10,50,5);
        predict=svrpredict(x,a,astar,b,x);
        %figure
        plot(x',predict','r')
        
    else
        [x,y]=find(img~=0);
        imshow(img)
        hold on
        x=x';y=y';
        %scatter(y,x);
        %[a,astar,b]=mysvr(x,y,ex,c,tol,max_passes)
        [a,astar,b]=svr(x,y,20,50,5);
        predict=svrpredict(x,a,astar,b,x);
        %figure
        plot(predict',x','r')
    end
    saveas(gcf,sprintf('figures\\fr00_10_50_5%d.png',i));
end
