%more hidden units, error picture
\documentclass[conference,onecolumn]{IEEEtran}
\usepackage[colorlinks=false]{hyperref}
\usepackage{CJK}
\usepackage{graphicx}
\usepackage[draft]{listings}
%\usepackage{graphicx}
%\usepackage{listings}
\usepackage{pgffor}
\usepackage{xcolor}
\setcounter{tocdepth}{5}
\lstset{language=Matlab}
\lstset{breaklines}
\lstset{extendedchars=false}
\lstset{
keywordstyle= \color{ blue!70},commentstyle=\color{red!50!green!50!blue!50},
rulesepcolor= \color{ red!20!green!20!blue!20}
}



% colorlinks,linkcolor=blue,anchorcolor=blue,citecolor=green
\begin{document}
%
% paper title
% can use linebreaks \\ within to get better formatting as desired
\title{Project I for Data Mining Course}


% author names and affiliations
% use a multiple column layout for up to three different
% affiliations
\author{\IEEEauthorblockN{Qi Lv}
\IEEEauthorblockA{
Department of Computer Science and Technology,\\ Shenzhen Graduate School, Tsinghua University, China\\
lvq13@mails.tsinghua.edu.cn }
}
% make the title area
\maketitle
\tableofcontents

%\section{Problem definition}
%% no \IEEEPARstart
%Topic: Training Feedforward Neural Networks
%
%Technique: BP Algorithm
%
%Task 1: XOR Problem
%4 input samples
%
%  $0 0  -> 0$
%
%  $1 0  -> 1$
%
%Task 2: Identity Function
%8 input samples
%
%$10000000   -> 10000000$
%
%$00010000   -> 00010000$
%
%$\cdots$
%
%Use 3 hidden units
%
%Deliverables:
%
%Report
%
%Code (any programming language, with detailed comments)
%
%Due: Sunday, 24 November
%
%Credit: 15\%
%


\section{Back propagation}
\begin{figure}[!hdp]
\centering
\includegraphics[scale=0.6]{pictures//back.eps}
\caption{back}
\end{figure}

At first we can recapitulate what is taught in the course about backpropagation. Part of the notation and formula are excerpts of the wikipedia\cite{wiki}.

The squared error function is:

$E = 1/2*(t-y)^2$

$  E =$ the squared error,

$t =$ the target output,

$y =$ the actual output neuron.

Therefore the error $E$, depends on the output y.

$y=\phi(net)$





\section{XOR problem}
\begin{figure}[!hdp]
\centering
\includegraphics[scale=0.6]{pictures//xorplot.eps}
\caption{Error surface}
\end{figure}
The xor problem is formulated simply in the table I. The design of network could be a two layer perceptron: one layer with two nodes for input, one layer with one node for output. However, the output cannot be linearly separated by this network architecture, because there is no line in the two dimensional space that can  separate the four points, as illustrated in fig 2.
\begin{table}[!hdp]
\caption{XOR problem}
\centering
\begin{tabular}{c c} \hline
Input Patterns & Output Patterns\\\hline
00& 0\\
01& 1\\
10& 1\\
11& 0\\\hline
\end{tabular}
\end{table}

This is because the two dimension has a VC dimension of 3, rendering the four points sometimes inseparable, like the XOR problem. If we add a third input, we can separate the 4 points in three dimension space, as fig \ref{fig:xor3} shows.

\begin{table}[!hdp]
\caption{Three input XOR problem}
\centering
\begin{tabular}{c c} \hline
Input Patterns & Output Patterns\\\hline
000& 0\\
010& 1\\
100& 1\\
111& 0\\\hline
\end{tabular}
\end{table}





\begin{figure}[!hdp]
\centering
\includegraphics[scale=1]{pictures//xor3.eps}
\caption{Three inputs xor problem}
\label{fig:xor3}
\end{figure}



\subsection{XOR code}

\subsubsection{Hidden layer with 2 nodes}
 w2 = 7.8398   -8.0958   -3.5966 (for bias)
\begin{figure}[!hdp]
\centering
\includegraphics{pictures//pic2hidden.1}
\caption{Task 1, 2 hidden units}
\end{figure}

\begin{figure}[!hdp]
\centering
\includegraphics[scale=0.6]{pictures//errorsurface.eps}
\caption{Error surface}
\end{figure}


\label{code:xor}
\begin{lstlisting}
clear;clc;
samples=[0,0;0,1;1,0;1,1];
samples=repmat(samples,1220,1);%[samples; samples; samples;samples...];
label=[0;1;1;0];
label=repmat(label,1220,1);%[label;label;label;label...];

w=randn(3,2)-0.5; %random initialization, so that the weight update will be not be the same
w2=randn(3,1)-0.5;
delta=1;
error=0;
for i=1:size(samples,1)
    i;
    in=samples(i,:)';
    in=[in;1];% add bias
    in1=w'*in;
    hid=sigmoid(in1); %nonlinear function
    hid=[hid;1]; %add bias
    in2=w2'*hid;
    out=sigmoid(in2);
    label(i);
    error=[error 1/2*(out-label(i))^2]; %sum((out-label(i)).^2)
    kesi=(out-label(i))*out*(1-out); %backpropagation kesi
    w2_inc=hid*kesi';
    kesi2=kesi*hid.*(1-hid).*w2;%backpropagation second layer
    kesi2=kesi2(1:end-1); %the last one is the bias node in the hidden layer, no input feed to it,no need for back propagation
    w_inc=in*kesi2';
    w2=w2-delta*w2_inc;
    w=w-delta*w_inc;
       if isnan(w(1)) %sometimes the w will get to the NaN value, this indicate an error and no further calculation need to be done
        break;
       end
end
error
error=zeros(100,100); %plot the error surface
m=w2;
for i=1:100
    for j=1:100
    in=[0,0;0,1;1,0;1,1];
    in=in';
    in=[in;ones(1,size(in,2))]; %add bias
    labels=[0;1;1;0];
    in1=w'*in;
    hid=sigmoid(in1);
    hid=[hid;ones(1,size(hid,2))]; %add bias
    m(1)=-w2(1)+2*w2(1)/100*i;%from -w2(1) to w2(1),100 points total
    m(2)=-w2(2)+2*w2(2)/100*j;
    in2=m'*hid;
    out=sigmoid(in2);
    error(i,j)=sum((out-labels').^2);
    end
end
x=linspace(-w2(1),w2(1),100);
y=linspace(-w2(1),w2(1),100);
surf(x,y,error);
xlabel('w2(1)');ylabel('w2(2)');zlabel('error');
title('Error surface with different weight')
saveas(gcf,'pictures\errorsurface.eps');


\end{lstlisting}

\subsubsection{Hidden layer with 4 nodes}
4
\label{code:xor}
\begin{lstlisting}

clear;clc;
samples=[0,0;0,1;1,0;1,1];
samples=repmat(samples,1220,1);%[samples; samples; samples;samples...];
label=[0;1;1;0];
label=repmat(label,1220,1);%[label;label;label;label...];

w=randn(3,4)-0.5; %random initialization, so that the weight update will be not be the same
w2=randn(5,1)-0.5;
delta=1;
error=0;
for i=1:size(samples,1)
    i;
    in=samples(i,:)';
    in=[in;1];% add bias
    in1=w'*in;
    hid=sigmoid(in1); %nonlinear function
    hid=[hid;1]; %add bias
    in2=w2'*hid;
    out=sigmoid(in2);
    label(i);
    error=[error 1/2*(out-label(i))^2]; %sum((out-label(i)).^2)
    kesi=(out-label(i))*out*(1-out); %backpropagation kesi
    w2_inc=hid*kesi';
    kesi2=kesi*hid.*(1-hid).*w2;%backpropagation second layer
    kesi2=kesi2(1:end-1); %the last one is the bias node in the hidden layer, no input feed to it,no need for back propagation
    w_inc=in*kesi2';
    w2=w2-delta*w2_inc;
    w=w-delta*w_inc;
       if isnan(w(1)) %sometimes the w will get to the NaN value, this indicate an error and no further calculation need to be done
        break;
       end
end


\end{lstlisting}
\subsubsection{XOR code, batch learning with LBFGS}

fujkvhj
\label{code:xor}
\begin{lstlisting}

\end{lstlisting}

\section{Identity Function}
\begin{table}[!hdp]
\caption{Identity Function}
\centering
\begin{tabular}{c c} \hline
Input Patterns & Output Patterns\\\hline
10000000& 10000000\\
01000000&01000000\\
00100000&00100000\\
00010000& 00010000\\
...&...\\
\hline
\end{tabular}
\end{table}

\begin{figure}
\centering
\includegraphics{pictures//pic2.1}
\caption{Task 2}
\end{figure}

\subsection{Code}

%\begin{figure}
%\centering
%\includegraphics[width=13cm]{pictures//knearaccu}
% \caption{K-near neighbors accuracy changes with neighbors number}
% \label{fig:knearaccu}
%\end{figure}
%
%\label{code:pre}
%\begin{lstlisting}
%clear
%load c
%load xls
%[~,useful,~]=unique(a(:,1));
%a=a(useful,:);
%%outlier
%kesi=std(a);
%average=mean(a);
%count=0;
%id=[];
%col=[];
%idnum=[];
%p=struct([]);
%for i=1:size(a,1)
%    for j=1:size(a,2)
%        d(j).name=c(j);
%        d(j).var=a(i,j);
%    end
%    p=[p;d];
%end
%for i=1:size(a,1)
%    for j=2:size(a,2)
%        if a(i,j)>(average(j)+3*kesi(j))|a(i,j)<(average(j)-3*kesi(j))
%            id=[id,a(i,1)];
%            idnum=[idnum,i];
%            col=[col,j];
%            count=count+1;
%        end
%    end
%end
%stem((bsxfun(@minus,a,average))')
%hold on
%errorbar(zeros(size(average)),3*kesi,'xr')
%axis([0 39 -1000 2000])
%varname=struct([]);
%for j=1:size(a,2)
%    varname=[varname struct('name',c(j))];
%end
%for i=1:size(a,2)
%text(i-0.3,-1000,varname(i).name')
%end
%saveas(gcf,'pictures\std.png')
%saveas(gcf,'pictures\std.fig')
%
%b=a;
%b(idnum,2:end)=normalise(a(idnum,2:end),1);
%subplot(2,1,1);
%plot(a(idnum,:)');
%for i=1:size(a,2)
%text(i-0.3,0,varname(i).name')
%end
%subplot(2,1,2)
%plot(b(idnum,:)');
%for i=1:size(a,2)
%text(i-0.3,0,varname(i).name')
%end
%saveas(gcf,'pictures\nor.png')
%saveas(gcf,'pictures\norm.fig')
%\end{lstlisting}



% trigger a \newpage just before the given reference
% number - used to balance the columns on the last page
% adjust value as needed - may need to be readjusted if
% the document is modified later
%\IEEEtriggeratref{8}
% The "triggered" command can be changed if desired:
%\IEEEtriggercmd{\enlargethispage{-5in}}

% references section

% can use a bibliography generated by BibTeX as a .bbl file
% BibTeX documentation can be easily obtained at:
% http://www.ctan.org/tex-archive/biblio/bibtex/contrib/doc/
% The IEEEtran BibTeX style support page is at:
% http://www.michaelshell.org/tex/ieeetran/bibtex/
%\bibliographystyle{IEEEtran}
% argument is your BibTeX string definitions and bibliography database(s)
%\bibliography{IEEEabrv,../bib/paper}
%
% <OR> manually copy in the resultant .bbl file
% set second argument of \begin to the number of references
% (used to reserve space for the reference number labels box)

\begin{thebibliography}{1}

\bibitem{IEEEhowto:kopka}
H.~Kopka and P.~W. Daly, \emph{A Guide to \LaTeX}, 3rd~ed.\hskip 1em plus
  0.5em minus 0.4em\relax Harlow, England: Addison-Wesley, 1999.
  \bibitem{wiki}
  http://en.wikipedia.org/wiki/Backpropagation
  \bibitem{saecode}
  http://deeplearning.stanford.edu/wiki/index.php/Exercise:\_Implement\_deep\_networks\_for\_digit\_classification
\bibitem{matlab}
http://cda.psych.uiuc.edu/matlab\_class\_material/data\_analysis.pdf
\end{thebibliography}


% that's all folks
\end{document}


