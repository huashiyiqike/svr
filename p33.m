file=dir('Radius Estimation\*.bmp');
for i=4
    tmp=['Radius Estimation\' file(i).name];
    img=imread(tmp);
    [y,x]=find(img~=0);
    imshow(img)
    hold on
    ypre=y;
    y=-x'.^2-y'.^2;
    x=[-2*x -2*ypre ];
    x=x';
    if i==1
        [a,astar,b]=svr(x,y,900,0.5,300); %variance 100 r 95 10%
    elseif i==2
        [a,astar,b]=svr(x,y,200,0.5,500);%variance 16 r 100  40%
    elseif i==3
        [a,astar,b]=svr(x,y,100,0.05,100);%variance 1 r 100 80%
    elseif i==4
       % [a,astar,b]=svr(x,y,0,0.7,10);
        [a,astar,b]=svr(x,y,500,0.5,100); %variance 25 r 102  20%
    elseif i==5
        [a,astar,b]=svr(x,y,400,0.5,400); %variance 4 r 102 60%
    elseif i==6
        [a,astar,b]=svr(x,y,400,0.9,300); %variance 9 r 95 40%
    end
    num=max(size(a,1),size(a,2));
    left=zeros(size(x,1),1);
    for k=1:num
        left=bsxfun(@plus,left,(a(k)-astar(k))*x(:,k));%left+(a(k)-astar(k))*x(:,k);
    end
    r=sqrt(left(1)^2+left(2)^2-b)
    predict=svrpredict(x,a,astar,b,x);
    ang=0:0.01:2*pi;
    xp=r*cos(ang);
    yp=r*sin(ang);
    plot(left(1)+xp,left(2)+yp,'r');
    hold off%
    tmp
    %     plot(x,y,'o');hold on;
    %     plot(x,predict,'or');
  %  pause
    %     figure
    %    plot(x',predict','r')
    
    
    saveas(gcf,['figures\\lsq' sprintf('%d',i)  file(i).name]);
end


%     scatter(x(1,:),y);hold on
%     scatter(x(2,:),y,'r');hold off
%     figure
%     ff=svrpredict(x,a,astar,b,x);
%      scatter(x(1,:),ff);hold on
%     scatter(x(2,:),ff,'r')

%     left=zeros(size(x,1),size(x,2));
%     for k=1:num
%         left=left+repmat((a(k)-astar(k))*x(:,k),1,size(x,2));
%     end
%
