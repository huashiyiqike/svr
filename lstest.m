for k=-20:10:20
x=1:30;
y=-k*x+1*rand(1,30);
p3=polyfit(x,y,3);
plot(x,y,'r*');hold on
plot(1:30,polyval(p3,x));
end
hold off
for i=1:6
    tmp=sprintf('Angle Estimation\\fur00%d.bmp',i);
    img=imread(tmp);
    [x,y]=find(img~=0);
    imshow(img)
    hold on
    x=x';y=y';
    %scatter(y,x);
    %[a,astar,b]=mysvr(x,y,ex,c,tol,max_passes)
     p3=polyfit(x,y,3);
     y=polyval(p3,x);
    %figure
    plot(y',x','r')
    saveas(gcf,sprintf('figures\\fur00_1_500_5ls%d.png',i));
    pause
end
for i=1:6
        tmp=sprintf('Angle Estimation\\fr00%d.bmp',i);
        img=imread(tmp);
     if i<5
        [y,x]=find(img~=0);
        imshow(img)
        hold on
        x=x';y=y';
        %scatter(y,x);
        %[a,astar,b]=mysvr(x,y,ex,c,tol,max_passes)
        p3=polyfit(x,y,3);
        y=polyval(p3,x);
        %figure
        plot(x',y','r')
        pause
    else
        [x,y]=find(img~=0);
        imshow(img)
        hold on
        x=x';y=y';
        %scatter(y,x);
        %[a,astar,b]=mysvr(x,y,ex,c,tol,max_passes)
        p3=polyfit(x,y,3);
        y=polyval(p3,x);
        %figure
        plot(y',x','r')
     end
    pause
    saveas(gcf,sprintf('figures\\fr00_10_50_5ls%d.png',i));
end
