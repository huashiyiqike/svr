function ff=svrpredict(x,a,astar,b,test,varargin)
if isempty(varargin)
    fff=@inner;
elseif strcmp(varargin(1),'gauss')==1
    fff=@gauss;
    kesi22=varargin{2};
end
num=max(size(a,1),size(a,2));
ff=zeros(1,size(test,2));
% for k=1:num
%     ff=ff+(a(k)-astar(k))*fff(x(:,k)',test);%(x(:,k)'*test);
% end
left=zeros(size(x,1),1);
for k=1:num
    left=bsxfun(@plus,left,(a(k)-astar(k))*x(:,k));%left+(a(k)-astar(k))*x(:,k);
end
ff=left'*test;

ff=ff+b;
    function y=inner(a,b)
        y=a*b;
    end
    function y=gauss(a,b)
        y=exp(-sum((a-b).^2)/(2*kesi22^2));
    end
end