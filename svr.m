function [a,astar,b]=svr(x,y,ex,c,max_passes,varargin)
%ex very important, if the variance is big, it should be set high, or set
%high ex will also help.
if isempty(varargin)
    fff=@inner;
elseif strcmp(varargin(1),'gauss')==1
    fff=@gauss;
    kesi22=varargin{2};
end
DEBUG=0;
num=size(x,2);
a=zeros(num,1);
astar=zeros(num,1);
b=0;
passes=0;
flag=0;
e=zeros(num,1);
flagbest=0;
best=1e99999999;
while passes<max_passes
    passes
    %  pause
    num_changed_alphas=0;
    for i=1:num
        f2=0;
        for k=1:num
            f2=f2+(a(k)-astar(k))*fff( x(:,k)',x(:,i) );%(x(:,k)'*x(:,i));%
        end
        f2=f2+b;
        e(i)=f2-y(i);
        %         diary('log.txt')
        %         disp('e(i)');  e(i)
        %         disp('a(i)'); a(i)
        %         disp('astar(i)'); astar(i)
        %         i=i
        %         diary off
        if DEBUG,e(i), end
        if e(i)<ex&&e(i)>-ex&&a(i)<1e-2&&astar(i)<1e-2 ,continue,end  % a(i)~=0&&astar(i)~=0
                    j=ceil(rand(1,1)*num);%mod(i+10,num)+1;%
                    while j==i
                        j=ceil(rand(1,1)*num);
                    end
        %j=mod(i^2,num)+1;
        aiold=a(i);aistarold=astar(i);
        ajstarold=astar(j);ajold=a(j);
        if DEBUG,astar(j),ajold ,end
        f=0;
        for k=1:num
            f=f+(a(k)-astar(k))*fff( x(:,k)',x(:,j) );%(x(:,k)'*x(:,j));
        end
        f=f+b; e(j)=f-y(j);
        if e(j)<ex &&e(j)>-ex&&a(j)<1e-2&&astar(j)<1e-2 ,continue;end
        %             e(i),e(j)
        %             i,j
        yx=a(i)-astar(i)+a(j)-astar(j);
        eta=2*fff( x(:,i)',x(:,j) )-fff(x(:,j)',x(:,i))-fff(x(:,j)',x(:,j));%x(:,i)'*x(:,j)-x(:,i)'*x(:,i)-x(:,j)'*x(:,j);
        if eta>=0
            continue
        end
        eta=-eta;
        flag=1;
        %1 ai,aj
        a(j)=ajold+(e(i)-e(j))/eta;
        if DEBUG, a(j),end
        L=max(0,yx-c);H=min(c,yx);
        if H<=L,flag=0;
        else
            if a(j)>H, a(j)=H;
            elseif a(j)<L,  a(j)=L;
            end
            a(i)=yx-a(j);
            if DEBUG, a(i),end
            if a(i)>H||a(i)<L||a(i)==0||a(j)==0%||(a(i)==aiold&&a(j)==ajold)
                flag=0;
            end
        end
        if flag==1,astar(i)=0;astar(j)=0;  end
        if flag==0,     a(i)=aiold;astar(i)=aistarold;
            astar(j)=ajstarold;a(j)=ajold;end
        %2 ai  ajstar
        if ~flag
            flag=2;
            astar(j)=ajstarold-(e(i)-e(j)+2*ex)/eta;
            if DEBUG,astar(j),end
            H=min(c,yx+c);L=max(0,yx);
            if H<=L,flag=0;
            else
                if astar(j)>H, astar(j)=H;
                elseif astar(j)<L, astar(j)=L;
                end
                a(i)=yx+astar(j);
                if DEBUG, a(i),end
                if a(i)<L||a(i)>H||a(i)==0||astar(j)==0%||(a(i)==aiold&&astar(j)==ajstarold)
                    flag=0;
                end
            end
        end
        if flag==2,astar(i)=0;a(j)=0;end
        if flag==0,     a(i)=aiold;astar(i)=aistarold;
            astar(j)=ajstarold;a(j)=ajold;end
        %3 aistar,aj
        if ~flag
            flag=3;
            a(j)=ajold+(e(i)-e(j)-2*ex)/eta;
            if DEBUG,a(j),end
            H=min(c,-yx+c);L=max(0,-yx);
            if H<=L,flag=0;
            else
                if a(j)>H, a(j)=H;
                elseif a(j)<L,a(j)=L;
                end
                astar(i)=a(j)-yx;
                if DEBUG, astar(i),end
                if astar(i)<L||astar(i)>H||astar(i)==0||a(j)==0%||(a(j)==ajold&&astar(i)==aistarold)
                    flag=0;
                end
            end
        end
        if flag==3,a(i)=0;astar(j)=0;end
        if flag==0,     a(i)=aiold;astar(i)=aistarold;
            astar(j)=ajstarold;a(j)=ajold;end
        %4 aistar ajstar
        if ~flag
            flag=4;
            astar(j)=ajstarold-(e(i)-e(j))/eta;
            if DEBUG,astar(j),end
            H=min(c,-yx);L=max(0,-yx-c);
            if H<=L,flag=0;
            else
                if astar(j)>H, astar(j)=H;
                elseif astar(j)<L,astar(j)=L;
                end
                astar(i)=-a(j)-yx;
                if DEBUG, astar(i),end
                if astar(i)<L||astar(i)>H||astar(i)==0||astar(j)==0%||(astar(i)==aistarold&&astar(j)==ajstarold)
                    flag=0;
                end
            end
        end
        if flag==4,a(i)=0;astar(j)=0;end
        if flag==0,     a(i)=aiold;astar(i)=aistarold;
            astar(j)=ajstarold;a(j)=ajold;end
        
        if flag==1
            ff=0;
            for k=1:num
                ff=ff+(a(k)-astar(k))*fff( x(:,k)',x(:,i) );%*(x(:,k)'*x(:,i));
            end
            b1=y(i)-ex-ff;
            for k=1:num
                ff=ff+(a(k)-astar(k))*fff( x(:,k)',x(:,j) );%*(x(:,k)'*x(:,j));
            end
            b2=y(j)-ex-ff;
            b=(b1+b2)/2;
        elseif flag==2
            ff=0;
            for k=1:num
                ff=ff+(a(k)-astar(k))*fff( x(:,k)',x(:,i) );%*(x(:,k)'*x(:,i));
            end
            b1=y(i)-ex-ff;
            ff=0;
            for k=1:num
                ff=ff+(a(k)-astar(k))*fff( x(:,k)',x(:,j) );%*(x(:,k)'*x(:,j));
            end
            b2=y(j)+ex-ff;
            b=(b1+b2)/2;
            
        elseif flag==3
            ff=0;
            for k=1:num
                ff=ff+(a(k)-astar(k))*fff( x(:,k)',x(:,j) );%*(x(:,k)'*x(:,j));
            end
            b1=y(j)-ex-ff;
            ff=0;
            for k=1:num
                ff=ff+(a(k)-astar(k))*fff( x(:,k)',x(:,i) );%*(x(:,k)'*x(:,i));
            end
            b2=y(i)+ex-ff;
            b=(b1+b2)/2;
        elseif flag==4
            ff=0;
            for k=1:num
                ff=ff+(a(k)-astar(k))*fff( x(:,k)',x(:,j) );%*(x(:,k)'*x(:,j));
            end
            b1=y(j)+ex-ff;
            ff=0;
            for k=1:num
                ff=ff+(a(k)-astar(k))*fff( x(:,k)',x(:,i) );%*(x(:,k)'*x(:,i));
            end
            b2=y(i)+ex-ff;
            b=(b1+b2)/2;
        end
        %        predict=mysvrpredict(x,a,astar,b,x);
        %        max(predict-y)
        %                      diary('log2.txt')
        %                      a
        %                      astar
        %                      diary off
        %         plot(x,y,'o');hold on;
        %         plot(x,predict,'r');
        %         hold off
        %           pause
    end
    if num_changed_alphas==0
        passes=passes+1;
    else
        passes=0;
    end
    
    predict=svrpredict(x,a,astar,b,x);
    tmpp=sum(sum((predict-y).^2))
    if tmpp<best
        flagbest=1;
        abest=a;
        astarbest=astar;
        bbest=b;
        best=tmpp;
    end
%     plot(x,y,'o');hold on;
%     plot(x,predict,'or');
%     hold off
%     pause
    num_changed_alphas=num_changed_alphas+1;
end
    function y=inner(a,b)
        y=a*b;
    end
    function y=gauss(a,b)
        y=exp(-sum((a-b).^2)/(2*kesi22^2));
    end
if flagbest
    a=abest;
    astar=astarbest;
    b=bbest;
end
end

