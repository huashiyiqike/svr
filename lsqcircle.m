% for k=-20:10:20
% x=1:30;
% y=-k*x+1*rand(1,30);
% p3=polyfit(x,y,3);
% plot(x,y,'r*');hold on
% plot(1:30,polyval(p3,x));
% end
% hold off
file=dir('Radius Estimation\*.png');
for i=1:6
    tmp=['Radius Estimation\' file(i).name];
    img=imread(tmp);
    [x,y]=find(img~=0);
    imshow(img)
    hold on
    ypre=y;
    disp('**')
    max(x)-min(x);
    y=-x'.^2-y'.^2;
    x=[-2*x -2*ypre ];
    x=x';
    p=polyfitn([x(1,:)',x(2,:)'],y',1);
    % p.Coefficients
    r=sqrt(p.Coefficients(1)^2+p.Coefficients(2)^2-p.Coefficients(3));
    ang=0:0.01:2*pi;
    xp=r*cos(ang);
    yp=r*sin(ang);
    plot(p.Coefficients(1)+xp,p.Coefficients(2)+yp,'r');
    pause
    
    %scatter(y,x);
    %[a,astar,b]=mysvr(x,y,ex,c,tol,max_passes)
    %      p3=polyfit(x,y,3);
    %      y=polyval(p3,x);
    %figure
    %     plot(y',x','r')
     saveas(gcf,['figures\\lsqreal'  file(i).name]);
    %     pause
end

