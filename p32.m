for i=1:6
    tmp=sprintf('Angle Estimation\\fur00%d.bmp',i);
    img=imread(tmp);
    [x,y]=find(img~=0);
    imshow(img)
    hold on
    x=x';y=y';
    %scatter(y,x);
    %[a,astar,b]=mysvr(x,y,ex,c,tol,max_passes)
    if i<5
        [a,astar,b]=svr(x,y,1,500,5);
    else
        [a,astar,b]=svr(x,y,20,500,5);
    end
    predict=svrpredict(x,a,astar,b,x);
    %figure
    plot(predict',x','r')
    saveas(gcf,sprintf('figures\\fur00_1_500_5%d.png',i));
end
