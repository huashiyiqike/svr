function ff=mysvrpredict(x,a,astar,b,test)
num=max(size(a,1),size(a,2));
ff=zeros(1,size(test,2));
for k=1:num
    ff=ff+(a(k)-astar(k))*(x(:,k)'*test);
end
ff=ff+b;
end