for k=-20:10:20
x=[1:100;1:100];
y=-k*1:100+50*rand(1,100);
[a,astar,b]=svr(x,y,0,100,10);
test=x;
predict=svrpredict(x,a,astar,b,test);
plot(x(1,:),y,'k*');hold on;
plot(x(1,:),predict,'r');
hold off
pause
end